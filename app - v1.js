const http = require('http');
const path = require('path');
const fs = require('fs');
const mime = require('mime');

http.createServer().on('request',(req,res)=>{
    console.log(11);
    req.url = req.url.toLowerCase();
    req.method = req.method.toLowerCase();
    console.log(req.url,req.method);
    if((req.url == "/" || req.url == "/index") && req.method == "get"){
        console.log(22);
        fs.readFile(path.join(__dirname,"views","index.html"),(err,data)=>{
            if(err) throw err;
            res.end(data);
        });
    }else if( req.url == "/item" && req.method == "get"){
        fs.readFile(path.join(__dirname,"views","details.html"),(err,data)=>{
            if(err) throw err;
            res.end(data);
        });
    }else if( req.url == "/submit" && req.method == "get"){
        fs.readFile(path.join(__dirname,"views","submit.html"),(err,data)=>{
            if(err) throw err;
            res.end(data);
        });
    }else if( req.url == "/add" && req.method == "post"){
        
    }else if( req.url.startsWith("/resources") && req.method == "get"){
        res.setHeader("Content-Type",mime.getType(req.url));
        fs.readFile(path.join(__dirname,req.url),(err,data)=>{
            if(err) throw err;
            res.end(data);
        });
    }else{
        res.writeHead(404,"Not Found",{
            "Content-Type":"text/html;charset=utf-8"
        });
    }
}).listen(80,()=>{
    console.log("服务启动成功");
})