/*
获取querystring参数
1. 使用url内置模块.
2. 调用parse()方法，将url格式化为1个对象.
3. obj.query.key 获取.


跳转:
302 Found
setHeader("location","newP")

*/


const http = require('http');
const path = require('path');
const fs = require('fs');
const mime = require('mime');
const url = require('url');


http.createServer().on('request', (req, res) => {
    res.render = (fileName, res) => {
        fs.readFile(fileName, (err, data) => {
            if (err) {
                res.writeHead(500, "Internal Server Error", { "Content-Type": "text/html;charset=utf-8" });
                res.end();
            }
            res.setHeader("Content-Type", mime.getType(fileName));
            res.end(data);
        });
    };
    req.url = req.url.toLowerCase();
    req.method = req.method.toLowerCase();
    if ((req.url == "/" || req.url == "/index") && req.method == "get") {
        res.render(path.join(__dirname, "views", "index.html"), res);
    } else if (req.url == "/item" && req.method == "get") {
        res.render(path.join(__dirname, "views", "details.html"), res);
    } else if (req.url == "/submit" && req.method == "get") {
        res.render(path.join(__dirname, "views", "submit.html"), res);
    } else if (req.url.startsWith("/add?") && req.method == "get") {
        const urlObj = url.parse(req.url, true);
        fs.readFile(path.join(__dirname, "data/news.json"), "utf8", (err, data) => {
            if (err && err.code != "ENOENT") { throw err; }
            var list = JSON.parse(data || "[]");
            list.push(urlObj.query);
            fs.writeFile(path.join(__dirname, "data/news.json"), JSON.stringify(list), (err) => {
                if (err) { throw err; }
                res.writeHead(302, "Found", {
                    "location": "/"
                });
                res.end();
            });
        });
    } else if (req.url == "/add" && req.method == "post") {

    } else if (req.url.startsWith("/resources") && req.method == "get") {
        res.render(path.join(__dirname, req.url), res);
    } else {
        res.writeHead(404, "Not Found", {
            "Content-Type": "text/html;charset=utf-8"
        });
    }
}).listen(80, () => {
    console.log("服务启动成功");
});